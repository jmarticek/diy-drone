# DIY Drone Quadrocopter
## Complete guide to your own quadrocopter

## *Prerequisites*
*Skills & tools*
- Building
- Soldering
- 3D printer / 3D printing service available
- Soldering iron

*Materials*
- Arduino nano
- M3 screws
- RC transmitter & reciever
- ESC & brushless motors
- Propellers
- MPU-6050 module